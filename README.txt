DESCRIPTION
-----------
This module provides ways to index entity while Update and insert.

INSTALLATION
------------
Step 1 ->Install this module.
Step 2 -> Go to URL/admin/config/search/apachesolr/solr-index-content-typee 
Check content type which you want to index.
Install the module as normal,
but make sure that Apachesolr and (optionally)
Apachesolr Views modules are installed correctly.
And the Solr indexing.
